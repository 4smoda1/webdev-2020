# Индивидуальные задания

**2) Сайт-визитка (Google Sites) -** https://gitlab.com/4smoda1/webdev-2020/-/tree/Kurilenko/02/README.md


**4) Сайт-визитка на разных конструкторах-сайтов -** https://gitlab.com/4smoda1/webdev-2020/-/tree/Kurilenko/04/README.md


**6) Сайт-визитка WordPress (local, openserver) -** https://gitlab.com/4smoda1/webdev-2020/-/tree/Kurilenko/06/README.md


**7) Сайт-визитка на Wordpress Gutenberg/Wordpress Elementor и на сайте WordPress.com -** https://4smoda1.com/Kurilenko/webdev-2020/-/tree/Kurilenko/07/README.md


**8) Мой GLOSSARY -** https://gitlab.com/4smoda1/webdev-2020/-/tree/Kurilenko/08/GLOSSARY.md

